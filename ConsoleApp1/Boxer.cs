﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
 
    public class Boxer
    {
        public string Name { get; set; }
        public int Health { get; set; }
        public int Strength { get; set; }
        public int Stamina { get; set; }
        public int Victories { get; set; }


    }
    
}
