﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Fight
    {
        public Boxer boxer1 { get; set; }
        public Boxer boxer2 { get; set; }

        public int TotalRounds { get; set; }
        public int CurrentRound { get; set; }
    }
}
